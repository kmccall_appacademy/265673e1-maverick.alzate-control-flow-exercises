# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  destroyed_string = ""
  str.each_char { |ch| destroyed_string << ch if ch != ch.downcase }
  destroyed_string
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length / 2
  str.length % 2 == 0 ? str[(middle - 1)..middle] : str[middle]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char { |ch| count += 1 if VOWELS.include? ch }
  count
end

# Return the factora of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each { |i| product *= i }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""

  arr.each_index do |i|
    if i < arr.length - 1
      string += arr[i].to_s + separator
    else
      string += arr[i].to_s
    end
  end

  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""

  count = 0
  while count < str.length
    new_str += (count + 1) % 2 == 0 ? str[count].upcase : str[count].downcase
    count += 1
  end

  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  sdrow = words.map { |word| word.length > 4 ? word.reverse : word }
  sdrow.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fb_arr = Array.new(n) { |i| "" }
  fb_arr.each_index do |i|
    if (i + 1) % 3 == 0 || (i + 1) % 5 == 0
      if (i + 1) % 3 == 0
        fb_arr[i] += "fizz"
      end
      if (i + 1) % 5 == 0
        fb_arr[i] += "buzz"
      end
    else
      fb_arr[i] = i + 1
    end
  end

  fb_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1

  (2..num / 2).each { |i| return false if num % i == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  fctrs = []
  (1..num).each { |i| fctrs.push(i) if num % i == 0 }
  fctrs
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_fctrs = []
  factors(num).each { |fctr| prime_fctrs.push(fctr) if prime?(fctr) }
  prime_fctrs
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_count = 0
  arr.each { |el| odd_count += 1 if el % 2 != 0 }

  if odd_count > 1
    arr.each { |el| return el if el % 2 == 0 }
  else
    arr.each { |el| return el if el % 2 != 0 }
  end
end
